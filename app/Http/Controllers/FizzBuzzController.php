<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class FizzBuzzController extends Controller
{
    public function index()
    {
        $output = [];
        for ($i = 1; $i <= 100; $i++) {
            if ($i % 3 == 0 && $i % 5 == 0) {
                $output[] = 'FizzBuzz';
            } elseif ($i % 3 == 0) {
                $output[] = 'Fizz';
            } elseif ($i % 5 == 0) {
                $output[] = 'Buzz';
            } else {
                $output[] = $i;
            }
        }
        return view('fizzbuzz', ['output' => $output]);
    }
}
