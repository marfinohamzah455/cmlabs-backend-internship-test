<!DOCTYPE html>
<html>
<head>
    <title>FizzBuzz</title>
</head>
<body>
    <h1>FizzBuzz Output</h1>
    <ul>
        @foreach ($output as $item)
            <li>{{ $item }}</li>
        @endforeach
    </ul>
</body>
</html>

